import 'dart:math';

import 'package:flutter/material.dart';

class AddDrop extends StatefulWidget {
  const AddDrop({super.key});

  @override
  State<AddDrop> createState() => _AddDrop();
}

class _AddDrop extends State<AddDrop> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: buildAppBar(),
      body: buildBackGround(),
    );
  }
}

AppBar buildAppBar() {
  return AppBar(
    backgroundColor: Colors.grey.shade800,
    elevation: 0,
    iconTheme: IconThemeData(color: Colors.yellow.shade700),
    title: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          'Add-Drop',
          style: TextStyle(
              fontSize: 36,
              fontWeight: FontWeight.bold,
              color: Colors.yellow.shade700),
        ),
      ],
    ),
  );
}

Widget buildBackGround() {
  return Scaffold(
    backgroundColor: Colors.grey.shade400,
    body: Container(
      padding: const EdgeInsets.all(10.0),
      alignment: Alignment.center,
      child: ListView(
        children: <Widget>[
          buildNotice(),
        ],
      ),
    ),
  );
}

Widget buildNotice() {
  return Center(
    child: SizedBox(
      height: 300,
      width: 300,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: Colors.white,
        ),
        child: Padding(
          padding: const EdgeInsets.all(5.0),
          child: Column(
            children: const <Widget>[
              Divider(
                color: Colors.transparent,
              ),
              Text(
                "Out Of Time",
                style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold),
              ),
              Icon(
                Icons.close,
                color: Colors.red,
                size: 220.0,
              ),
            ],
          ),
        ),
      ),
    ),
  );
}
