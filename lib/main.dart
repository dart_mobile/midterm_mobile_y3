import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:regflutter/AddDrop.dart';
import 'package:device_preview/device_preview.dart';

import 'TimeTable.dart';
import 'Enroll.dart';


void main() {
  runApp(
    DevicePreview(
      enabled: true,
      builder: (context) => const MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        home: RegPage(),
      ),
    ),
  );
}

class RegPage extends StatefulWidget {
  const RegPage({super.key});

  @override
  State<RegPage> createState() => _RegPage();
}

class _RegPage extends State<RegPage> {
  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    final mainFontSize = screenWidth * 0.048;
    final detailFontSize = screenWidth * 0.038;
    final contentHeight = screenHeight * 0.085;

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: buildAppbar(mainFontSize, detailFontSize, contentHeight),
      drawer: buildDrawer(mainFontSize, detailFontSize, contentHeight),
      body: buildBackGround(mainFontSize, detailFontSize, contentHeight),
    );
  }

  AppBar buildAppbar(mainFontSize, detailFontSize, contentHeight) {
    return AppBar(
      backgroundColor: Colors.grey.shade800,
      elevation: 0,
      toolbarHeight: contentHeight,
      iconTheme: IconThemeData(
          color: Colors.yellow.shade700, size: mainFontSize * 1.8),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'Burapha',
            style: TextStyle(
                fontSize: mainFontSize * 1.8,
                fontWeight: FontWeight.bold,
                color: Colors.yellow.shade700),
          ),
        ],
      ),
    );
  }

  Drawer buildDrawer(mainFontSize, detailFontSize, contentHeight) {
    return Drawer(
      backgroundColor: Colors.grey.shade400,
      child: ListView(
        children: <Widget>[
          DrawerHeader(
            child: Column(
              children: const <Widget>[
                CircleAvatar(
                  radius: 65,
                  backgroundImage: NetworkImage(
                    'https://cdn-icons-png.flaticon.com/512/4140/4140048.png',
                  ),
                  backgroundColor: Colors.transparent,
                ),
              ],
            ),
          ),
          InkWell(
            splashColor: Colors.grey.shade500,
            onTap: () {
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const TimeTable()),
              );
            },
            child: Padding(
              padding: const EdgeInsets.only(top: 5, bottom: 5),
              child: ListTile(
                leading:
                    const Icon(Icons.schedule_outlined, color: Colors.black),
                title: Text(
                  "Time table",
                  style: TextStyle(
                    fontSize: detailFontSize * 1.5,
                    fontWeight: FontWeight.normal,
                  ),
                ),
              ),
            ),
          ),
          InkWell(
            splashColor: Colors.grey.shade500,
            onTap: () {
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const Enroll()),
              );
            },
            child: Padding(
              padding: const EdgeInsets.only(top: 5, bottom: 5),
              child: ListTile(
                leading: const Icon(CupertinoIcons.doc, color: Colors.black),
                title: Text(
                  "Enroll",
                  style: TextStyle(
                    fontSize: detailFontSize * 1.5,
                    fontWeight: FontWeight.normal,
                  ),
                ),
              ),
            ),
          ),
          InkWell(
            splashColor: Colors.grey.shade500,
            onTap: () {
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const AddDrop()),
              );
            },
            child: Padding(
              padding: const EdgeInsets.only(top: 5, bottom: 5),
              child: ListTile(
                leading:
                    const Icon(CupertinoIcons.doc_person, color: Colors.black),
                title: Text(
                  "Add-Drop",
                  style: TextStyle(
                    fontSize: detailFontSize * 1.5,
                    fontWeight: FontWeight.normal,
                  ),
                ),
              ),
            ),
          ),
          InkWell(
            splashColor: Colors.grey.shade500,
            onTap: () {},
            child: ListTile(
              leading:
                  const Icon(Icons.meeting_room_outlined, color: Colors.black),
              title: Text(
                "Reservation",
                style: TextStyle(
                  fontSize: detailFontSize * 1.5,
                  fontWeight: FontWeight.normal,
                ),
              ),
            ),
          ),
          InkWell(
            splashColor: Colors.grey.shade500,
            onTap: () {},
            child: Padding(
              padding: const EdgeInsets.only(top: 5, bottom: 5),
              child: ListTile(
                leading:
                    const Icon(CupertinoIcons.settings, color: Colors.black),
                title: Text(
                  "Setting",
                  style: TextStyle(
                    fontSize: detailFontSize * 1.5,
                    fontWeight: FontWeight.normal,
                  ),
                ),
              ),
            ),
          ),
          InkWell(
            splashColor: Colors.grey.shade500,
            onTap: () {},
            child: Padding(
              padding: const EdgeInsets.only(top: 5, bottom: 5),
              child: ListTile(
                leading: const Icon(Icons.logout_outlined, color: Colors.red),
                title: Text(
                  "LogOut",
                  style: TextStyle(
                      fontSize: detailFontSize * 1.5,
                      fontWeight: FontWeight.normal,
                      color: Colors.red),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildDivider() {
    return const Divider(
      color: Colors.transparent,
    );
  }

  Widget buildBackGround(mainFontSize, detailFontSize, contentHeight) {
    return Scaffold(
      backgroundColor: Colors.grey.shade400,
      body: Container(
        padding: const EdgeInsets.all(10.0),
        alignment: Alignment.center,
        child: ListView(
          children: <Widget>[
            buildUserProfile(mainFontSize, detailFontSize, contentHeight),
            const Divider(
              color: Colors.transparent,
            ),
            buildCardProfile(mainFontSize, detailFontSize, contentHeight),
            buildDivider(),
            buildCardOrganization(mainFontSize, detailFontSize, contentHeight),
            buildDivider(),
            buildTranscriptButton(mainFontSize, detailFontSize, contentHeight),
          ],
        ),
      ),
    );
  }

  Widget buildUserProfile(mainFontSize, detailFontSize, contentHeight) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(200.0),
      ),
      color: Colors.transparent,
      elevation: 0,
      child: Row(
        children: <Widget>[
          const CircleAvatar(
            minRadius: 40,
            backgroundImage: NetworkImage(
              'https://cdn-icons-png.flaticon.com/512/4140/4140048.png',
            ),
            backgroundColor: Colors.transparent,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Text(
                      "KITTIPON THAWEEALRB",
                      style: TextStyle(
                          fontSize: mainFontSize, fontWeight: FontWeight.bold),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 3),
                      child: IconButton(
                        icon: Icon(
                          Icons.edit,
                          size: mainFontSize,
                        ),
                        color: Colors.yellow.shade700,
                        splashColor: Colors.grey.shade500,
                        onPressed: () {},
                      ),
                    ),
                  ],
                ),
                Text(
                  "Informatics",
                  style: TextStyle(
                      fontSize: detailFontSize, color: Colors.grey.shade600),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget buildCardProfile(mainFontSize, detailFontSize, contentHeight) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      color: Colors.white,
      elevation: 5,
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Profile",
                  style: TextStyle(
                      fontSize: mainFontSize,
                      fontWeight: FontWeight.bold,
                      color: Colors.yellow.shade700),
                )
              ],
            ),
            const Divider(
              color: Colors.transparent,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Name (EN)",
                      style: TextStyle(
                          fontSize: detailFontSize,
                          fontWeight: FontWeight.normal,
                          color: Colors.grey.shade700),
                    ),
                    Text(
                      "KITTIPON THAWEELARB",
                      style: TextStyle(
                          fontSize: detailFontSize,
                          fontWeight: FontWeight.normal,
                          color: Colors.black),
                    )
                  ],
                )
              ],
            ),
            const Divider(
              color: Colors.transparent,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Name (TH)",
                      style: TextStyle(
                          fontSize: detailFontSize,
                          fontWeight: FontWeight.normal,
                          color: Colors.grey.shade700),
                    ),
                    Text(
                      "กิตติภณ ทวีลาภ",
                      style: TextStyle(
                          fontSize: detailFontSize,
                          fontWeight: FontWeight.normal,
                          color: Colors.black),
                    )
                  ],
                )
              ],
            ),
            const Divider(
              color: Colors.transparent,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  flex: 7,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Birthdate",
                        style: TextStyle(
                            fontSize: detailFontSize,
                            fontWeight: FontWeight.normal,
                            color: Colors.grey.shade700),
                      ),
                      Text(
                        "19 Oct 2001",
                        style: TextStyle(
                            fontSize: detailFontSize,
                            fontWeight: FontWeight.normal,
                            color: Colors.black),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 6,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Gender",
                        style: TextStyle(
                            fontSize: detailFontSize,
                            fontWeight: FontWeight.normal,
                            color: Colors.grey.shade700),
                      ),
                      Text(
                        "Male",
                        style: TextStyle(
                            fontSize: detailFontSize,
                            fontWeight: FontWeight.normal,
                            color: Colors.black),
                      )
                    ],
                  ),
                ),
              ],
            ),
            const Divider(
              color: Colors.transparent,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Nationality",
                      style: TextStyle(
                          fontSize: detailFontSize,
                          fontWeight: FontWeight.normal,
                          color: Colors.grey.shade700),
                    ),
                    Text(
                      "Thai",
                      style: TextStyle(
                          fontSize: detailFontSize,
                          fontWeight: FontWeight.normal,
                          color: Colors.black),
                    )
                  ],
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget buildCardOrganization(mainFontSize, detailFontSize, contentHeight) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      color: Colors.white,
      elevation: 5,
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Organization",
                  style: TextStyle(
                      fontSize: mainFontSize,
                      fontWeight: FontWeight.bold,
                      color: Colors.yellow.shade700),
                ),
              ],
            ),
            const Divider(
              color: Colors.transparent,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  flex: 7,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "User Type",
                        style: TextStyle(
                            fontSize: detailFontSize,
                            fontWeight: FontWeight.normal,
                            color: Colors.grey.shade700),
                      ),
                      Text(
                        "Student",
                        style: TextStyle(
                            fontSize: detailFontSize,
                            fontWeight: FontWeight.normal,
                            color: Colors.black),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 6,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "ID",
                        style: TextStyle(
                            fontSize: detailFontSize,
                            fontWeight: FontWeight.normal,
                            color: Colors.grey.shade700),
                      ),
                      Text(
                        "63160184",
                        style: TextStyle(
                            fontSize: detailFontSize,
                            fontWeight: FontWeight.normal,
                            color: Colors.black),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            const Divider(
              color: Colors.transparent,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Year",
                      style: TextStyle(
                          fontSize: detailFontSize,
                          fontWeight: FontWeight.normal,
                          color: Colors.grey.shade700),
                    ),
                    Text(
                      "3",
                      style: TextStyle(
                          fontSize: detailFontSize,
                          fontWeight: FontWeight.normal,
                          color: Colors.black),
                    ),
                  ],
                ),
              ],
            ),
            const Divider(
              color: Colors.transparent,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  flex: 7,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Faculty",
                        style: TextStyle(
                            fontSize: detailFontSize,
                            fontWeight: FontWeight.normal,
                            color: Colors.grey.shade700),
                      ),
                      Text(
                        "Informatics",
                        style: TextStyle(
                            fontSize: detailFontSize,
                            fontWeight: FontWeight.normal,
                            color: Colors.black),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 6,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Major",
                        style: TextStyle(
                            fontSize: detailFontSize,
                            fontWeight: FontWeight.normal,
                            color: Colors.grey.shade700),
                      ),
                      Text(
                        "B.Sc. (Computer Science)",
                        style: TextStyle(
                            fontSize: detailFontSize,
                            fontWeight: FontWeight.normal,
                            color: Colors.black),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            const Divider(
              color: Colors.transparent,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Email",
                      style: TextStyle(
                          fontSize: detailFontSize,
                          fontWeight: FontWeight.normal,
                          color: Colors.grey.shade700),
                    ),
                    Text(
                      "63160184@go.buu.ac.th",
                      style: TextStyle(
                          fontSize: detailFontSize,
                          fontWeight: FontWeight.normal,
                          color: Colors.black),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget buildTranscriptButton(mainFontSize, detailFontSize, contentHeight) {
    return InkWell(
      onTap: () {},
      splashColor: Colors.grey.shade500,
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
          side: BorderSide(color: Colors.yellow.shade700, width: 3),
        ),
        color: Colors.transparent,
        elevation: 5,
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                'Transcript & Advisory',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: mainFontSize,
                    color: Colors.yellow.shade700),
              )
            ],
          ),
        ),
      ),
    );
  }
}
